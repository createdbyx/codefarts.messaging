﻿namespace FileMessagesConsoleTester
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Threading;

    using Codefarts.Messaging;

    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                if (ShowMainMenu())
                {
                    return;
                }
            }
        }

        private static bool ShowMainMenu()
        {
            var index = ShowMenu(new[] { "1: Start server", "2: Start Client", "3: Flush messages", "4: Start new instance", "5: Quit" });
            switch (index)
            {
                case ConsoleKey.D1:
                    RunServer();
                    break;

                case ConsoleKey.D2:
                    RunClient();
                    break;

                case ConsoleKey.D3:
                    DeleteMessagesFolder();
                    break;

                case ConsoleKey.D4:
                    Process.Start(Environment.GetCommandLineArgs()[0].Replace(".vshost", string.Empty));
                    break;

                case ConsoleKey.D5:
                    return true;
            }

            return false;
        }

        private static void DeleteMessagesFolder()
        {
            try
            {
                var sourceFolder = Path.Combine(Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]), "Messages");
                var files = Directory.GetFiles(sourceFolder);
                foreach (var file in files)
                {
                    File.Delete(file);
                }
                Directory.Delete(sourceFolder);
            }
            catch
            {
            }
        }

        private static void RunServer()
        {
            var sourceFolder = Path.Combine(Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]), "Messages");
            Directory.CreateDirectory(sourceFolder);
            var reader = new FileMessageReader<MessageData>(sourceFolder, "Client[{0}].txt");
            var writer = new FileMessageWriter<MessageData>(sourceFolder, "Server[{0}].txt");

            var running = true;
            ThreadPool.QueueUserWorkItem(cb =>
            {
                while (running)
                {
                    try
                    {
                        var message = reader.ReadObject();
                        Console.WriteLine("Client {0}: {1}", message.Id, message.Message);
                    }
                    catch
                    {

                    }
                }

                running = true;
            });

            Console.Clear();
            while (running)
            {
                var line = Console.ReadLine();
                if (ProcessLine(line, "[SERVER]: ", writer))
                {
                    running = false;
                    break;
                }
            }            
        }

        private static void RunClient()
        {
            var sourceFolder = Path.Combine(Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]), "Messages");
            Directory.CreateDirectory(sourceFolder);
            var reader = new FileMessageReader<MessageData>(sourceFolder, "Server[{0}].txt");
            var writer = new FileMessageWriter<MessageData>(sourceFolder, "Client[{0}].txt");

            var running = true;
            ThreadPool.QueueUserWorkItem(cb =>
            {
                while (running)
                {
                    try
                    {
                        var message = reader.ReadObject();
                        Console.WriteLine("Server {0}: {1}", message.Id, message.Message);
                    }
                    catch
                    {

                    }
                }

                running = true;
            });

            Console.Clear();
            while (running)
            {
                var line = Console.ReadLine();
                if (ProcessLine(line, "[CLIENT]: ", writer))
                {
                    running = false;
                    break;
                }
            }   
        }

        private static bool ProcessLine(string line, string prefix, WriterBase<MessageData> writer)
        {
            line = line.Trim();
            if (line.StartsWith("-"))
            {
                switch (line.Substring(1).ToLower())
                {
                    case "quit":
                        return true;
                }
            }
            else
            {
                writer.WriteObject(new MessageData() { Message = prefix + line });
            }

            return false;
        }

        private static ConsoleKey ShowMenu(string[] options)
        {
            while (true)
            {
                Console.Clear();
                foreach (var option in options)
                {
                    Console.WriteLine(option);
                }

                var key = Console.ReadKey();
                switch (key.Key)
                {
                    case ConsoleKey.D0:
                    case ConsoleKey.D1:
                    case ConsoleKey.D2:
                    case ConsoleKey.D3:
                    case ConsoleKey.D4:
                    case ConsoleKey.D5:
                    case ConsoleKey.D6:
                    case ConsoleKey.D7:
                    case ConsoleKey.D8:
                    case ConsoleKey.D9:
                        var index = key.Key - ConsoleKey.D0;
                        if (index < 0 || index > 9)
                        {
                            continue;
                        }

                        return key.Key;
                }
            }
        }
    }

    [Serializable]
    public class MessageData
    {
        public string Message { get; set; }

        public int Id { get; set; }
    }
}
